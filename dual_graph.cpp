/*
 * Задача 4. Двудольный граф.
 * Требуется проверить заданный граф на двудольность.
 * Время - O(V+E). Память - O(V).
 */
#include <iostream>
#include <list>
#include <stack>
#include <vector>

using std::list;
using std::vector;

struct IGraph {
  virtual ~IGraph() {}

  virtual void AddEdge(int from, int to) = 0;

  virtual int VerticesCount() const = 0;

  virtual void GetNextVertices(int vertex,
                               std::vector<int>& vertices) const = 0;
  virtual void GetPrevVertices(int vertex,
                               std::vector<int>& vertices) const = 0;
};

struct ListGraph : public IGraph {
 private:
  std::vector<std::list<int> > adjacency_list;
  std::vector<std::list<int> > reverse_adjacency_list;

 public:
  ListGraph(int n) : adjacency_list(n), reverse_adjacency_list(n) {}
  ~ListGraph() = default;
  ListGraph(const IGraph* graph);
  void AddEdge(int from, int to) override;
  int VerticesCount() const override;
  void GetNextVertices(int vertex, std::vector<int>& vertices) const override;
  void GetPrevVertices(int vertex, std::vector<int>& vertices) const override;
};

bool isBipartite(const IGraph& graph);

int main() {
  int vertices, edges;
  std::cin >> vertices >> edges;
  ListGraph graph(vertices);

  for (int i = 0; i < edges; i++) {
    int start, end;
    std::cin >> start >> end;
    graph.AddEdge(start, end);
    graph.AddEdge(end, start);
  }

  if (isBipartite(graph))
    std::cout << "YES";
  else
    std::cout << "NO";

  return 0;
}

bool isBipartite(const IGraph& graph) {
  vector<int> colors(graph.VerticesCount(), -1);
  for (int start_vertex = 0; start_vertex < graph.VerticesCount();
       start_vertex++) {
    if (colors[start_vertex] != -1)
        continue;
    std::stack<std::pair<int, int>> stack; // Состояние это вершина + номер с которой продолжать
    stack.push({start_vertex, 0});
    colors[start_vertex] = 0;
    while (!stack.empty()) {
      std::pair<int, int> state = stack.top();
      stack.pop();
      vector<int> nextVertices;
      graph.GetNextVertices(state.first, nextVertices);
      for (int it = state.second; it < nextVertices.size(); it++) {
        if (colors[nextVertices[it]] == -1) {
          colors[nextVertices[it]] = (colors[state.first] + 1) % 2;
          stack.push({state.first, it + 1});
          stack.push({nextVertices[it], 0});
          break;
        } else if (colors[nextVertices[it]] != ((1 + colors[state.first]) % 2))
          return false;
      }
    }
  }
  return true;
}
void ListGraph::AddEdge(int from, int to) {
  ListGraph::adjacency_list[from].push_front(to);
  ListGraph::reverse_adjacency_list[to].push_front(from);
}

int ListGraph::VerticesCount() const {
  return ListGraph::adjacency_list.size();
}

void ListGraph::GetNextVertices(int vertex, vector<int>& vertices) const {
  for (auto it : ListGraph::adjacency_list[vertex]) {
    vertices.push_back(it);
  }
}

void ListGraph::GetPrevVertices(int vertex, vector<int>& vertices) const {
  for (auto it : ListGraph::reverse_adjacency_list[vertex]) {
    vertices.push_back(it);
  }
}

ListGraph::ListGraph(const IGraph* graph)
    : adjacency_list(graph->VerticesCount()) {
  vector<int> temp;
  for (int i = 0; i < adjacency_list.size(); i++) {
    temp.clear();
    graph->GetNextVertices(i, temp);
    adjacency_list[i].insert(adjacency_list[i].end(), temp.begin(), temp.end());
  }
}
